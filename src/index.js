import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './reducers'
import Routing from './app/routing'


render(
  <Provider store={store}>
  <div>
  <Routing/>
    </div>
  </Provider>,
  document.getElementById('app')
)
