import {get} from 'lodash';
export const add_item =(state={},payload)=>{
    return {
        ...state,
        item:[...get(state,"item",[]), payload.item]
    }
}

export const remove_item=(state={},payload)=>
{  
    const temp =get(state,"item",[])
    if(payload.index!=-1)
    {
        temp.splice(payload.index, 1)
    }
    return {
        ...state,
        item:temp
    }
}