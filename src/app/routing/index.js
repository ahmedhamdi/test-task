import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import App from '../index'
// import {Button} from 'antd';
import cart from '../cart';
class Routing extends Component {
    render() {
        return (
            <Router>
                <section>
            <Switch>
            <Route exact path="/" component={App} />
            <Route path="/cart" component={cart}/>
            </Switch>             
                </section>
                </Router>
         
        );
    }
}

export default Routing;