import React, { Component } from 'react';
import { mapDispatchToProps,mapstatetoprops } from './mapping';
import { connect } from 'react-redux';
import {NavLink,withRouter} from 'react-router-dom';
import { Row, Col,Button } from 'antd';

class App extends Component {

      adding(item)
    {
        this.props.addItem(item)
    }
    remove(element){
      const index=this.props.item.indexOf(element);
      this.props.removeItem(index)
    }
    render() {
        return (
            <div className="gutter-example">
    <Row gutter={16}>
      <Col className="gutter-row" span={6}>
        <div>
     <label>item1</label>
        <Button type="primary" onClick={this.adding.bind(this,"item1")}>add to cart</Button>
        <Button type="primary" onClick={this.remove.bind(this,"item1")} >remove</Button>
                </div>
      </Col>
      <Col span={6}>
        <div>
        <label>item2</label>
        <Button type="primary" onClick={this.adding.bind(this,"item2")}>add to cart</Button>
        <Button type="primary" onClick={this.remove.bind(this,"item2")} >remove</Button>

            </div>
      </Col>
      <Col span={6}>
        <div >
        <label>item3</label>
        <Button type="primary" onClick={this.adding.bind(this,"item3")}>add to cart</Button>
        <Button type="primary" onClick={this.remove.bind(this,"item3")} >remove</Button>

        </div>
      </Col>
      <Col span={6}>
        <div>
        <label>item4</label>
        <Button type="primary" onClick={this.adding.bind(this,"item4")}>add to cart</Button>
        <Button type="primary" onClick={this.remove.bind(this,"item4")} >remove</Button>

        </div>
      </Col>
      <NavLink to="/cart" >
                <Button>cart</Button>
                </NavLink>
    </Row>
    
     
      </div>
        );
    }
}

export default connect(mapstatetoprops,mapDispatchToProps)(withRouter(((App))));

